import { Box, Divider, Heading, HStack, Text, Spacer } from '@chakra-ui/react'
import { DeleteIcon, CalendarIcon, SettingsIcon } from '@chakra-ui/icons'
import { Event } from '../../../core/entities/Event'

export interface EventCardProps {
  event: Event
  onDelete?: (id: string) => void
  onModify?: (id: string) => void
  onSubscribe?: (event: Event) => void
}

export default function EventCard(props: EventCardProps) {
  const {event, onDelete, onModify, onSubscribe} = props;
  const {eventId, name, description, location, date} = event;

  return (
    <Box p={5} m={5} shadow='md' borderWidth='1px' backgroundColor="#5A4998" w="280px">
      <Heading fontSize='xl' color="#2EEFAA">#{eventId} - {name}</Heading>
      <Text mt={4} color="#DDD" wordBreak='break-all'>{description}</Text>
      <Divider m="8px 0"/>
      <Text color="lightgreen" fontSize={'0.9rem'} align="right">
        {location} - {date}
      </Text>
      <Box mt="10px">
        <HStack>
          <CalendarIcon mr="5px" color="cyan" cursor="pointer" onClick={() => onSubscribe?.(event)}/>
          <SettingsIcon mr="5px" color="greenyellow" cursor="pointer" onClick={() => onModify?.(eventId)}/>
          <Spacer/>
          <DeleteIcon mr="5px" color="tomato" cursor="pointer" onClick={() => onDelete?.(eventId)}/>
        </HStack>
      </Box>
    </Box>
  )
}
