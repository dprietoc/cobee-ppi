import React from 'react';
import {Center, Button} from '@chakra-ui/react';

interface ActionButtonProps {
  name: string
  onClick: () => void,
  color?: string
  background?: string
  fontSize?: string
  height?: string
  hover?: object
  margin?: string | {}
  width?: string | {}
}

export const ActionButton: React.FC<ActionButtonProps> = (props: ActionButtonProps) => {
  const {
    background,
    color,
    fontSize,
    height,
    hover,
    margin,
    name,
    onClick,
    width,
  } = props;

  return (
    <Center>
      <Button 
        background={background}
        color={color}
        fontSize={fontSize}
        h={height}
        margin={margin}
        onClick={onClick}
        w={width}
        _focus={{outline: 0}} 
        _hover={hover}
      >
        {name}
      </Button>
    </Center>
  );
};
