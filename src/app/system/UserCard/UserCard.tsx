import { Event } from '../../../core/entities/Event'
import { Box, Divider, Heading, Text, HStack } from '@chakra-ui/react'
import {DeleteIcon} from '@chakra-ui/icons';

export interface UserCardProps {
  userId: string;
  name?: string;
  email?: string;
  eventList?: Event[];
  onEventDelete?: (userId: string, eventId: string) => void
}

export default function UserCard(props: UserCardProps) {
  const {userId, name, email, eventList, onEventDelete} = props;

  return (
    <Box p={5} m={5} shadow='md' border='1px solid #5A4998' w="350px">
      <Heading fontSize='lg' mb="10px">
        #{userId} - {name} ({email})
      </Heading>
      <Divider />
      { eventList?.length ? eventList.map((event) => (
        <HStack key={event.eventId} m="1rem 0">
          <Text color="blue" fontSize={'0.8rem'}>
            {`#${event.eventId} - ${event.name} | ${event.date}@${event.location}`}
          </Text>
          <DeleteIcon mr="5px" color="tomato" cursor="pointer" onClick={() => onEventDelete?.(userId, event.eventId)}/>
        </HStack>
      )) : 
        <Box mt="10px">No events subscribed yet</Box>
      }
    </Box>
  )
}
