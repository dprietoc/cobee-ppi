import {Box, Text} from '@chakra-ui/react';

export interface MenuProps {
  options: string[]
  onOptionClick: (option: string) => void
}

export default function Menu({options, onOptionClick}: MenuProps) {
  return (
    <Box w="20%" h="100%" borderRight={'1.5px solid #777'} p="25px 0">
      <ul>
        {options.map((option) => (
          <Box key={option} cursor="pointer" onClick={() => onOptionClick(option)}>
            <Text fontSize={'1.2rem'} fontWeight="bolder" color="#5A4998" ml="10%" mb="2rem" _hover={{color: '#8377B0'}}>
              {option}
            </Text>
          </Box>
        ))}
      </ul>
    </Box> 
  )
};