import React, {useState} from 'react';
import {Flex, Box, Input, Heading} from '@chakra-ui/react';
import {Event} from '../../../core/entities/Event';
import {ActionButton} from '../../system/ActionButton/ActionButton';

const getCleanEvent = (): Event => ({
  eventId: '',
  name: '',
  description: '',
  location: '',
  date: '',
});

export interface EventFormProps {
  onClose: () => void
  onSubmit: (event: Event) => void
  event?: Event
}

export default function EventForm({onClose, onSubmit, event}: EventFormProps) {
  const [formData, setFormData] = useState<Event>(event ?? getCleanEvent());

  const handleFormSubmit = () => {
    if (!formData) { return; }
    onSubmit(formData);
    setFormData(getCleanEvent())
  }
  
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFormData({...formData, [event.target.id]: event.target.value});
  };

  return (
    <Box w="450px">
      <Flex direction="column" m="0 50px" border="2px dashed #5A4998" p="20px" h="auto" justify="center">
        <Heading fontSize="lg" mb="10px">
          {
            event?.name ? `Modify Event #${event.eventId}` : `Add new event`
          }
        </Heading>
        <Input type="text" placeholder='Insert event ID ' value={formData.eventId} 
          onChange={handleInputChange} id="eventId" mb="8px" disabled={!!event?.eventId}/>
        <Input type="text" placeholder='Insert event name ' value={formData.name} 
          onChange={handleInputChange} id="name" mb="8px"/>
        <Input type="text" placeholder='Insert event description ' value={formData.description} 
          onChange={handleInputChange} id="description" mb="8px"/>
        <Input type="text" placeholder='Insert event location ' value={formData.location} 
          onChange={handleInputChange} id="location" mb="8px"/>
        <Input type="text" placeholder='Insert event date ' value={formData.date} 
          onChange={handleInputChange} id="date" mb="8px"/>
        <ActionButton name={event?.name ? 'Modify' : 'Add'} width="100%" color="white" hover={{backgroundColor: '#3D307B'}}
          background='#5A4998' height='50px' margin={'12px 0'} onClick={handleFormSubmit}/>
        <ActionButton name="Close" fontSize="0.9rem" width="100%" color="tomato" onClick={onClose} hover={{border: '1px solid tomato'}}/>  
      </Flex>
    </Box>
  );
};
