import {useEffect} from 'react';
import {useSelector} from 'react-redux';
import {State} from '../../reducers/menuReducer';
import {MockUserActionsRepository} from '../../../core/drivers/repositories/userActions/MockUserActionsRepository';
import {MockEventActionsRepository} from '../../../core/drivers/repositories/eventActions/MockEventActionsRepository';
import {LocalStorageRepository} from '../../../core/drivers/repositories/LocalStorageRepository';
import UserSection from '../userSection/UserSection';
import EventSection from '../eventSection/EventSection';
import useEventActionProvider from '../../../core/drivers/hooks/useEventActionProvider';
import useUserActionProvider from '../../../core/drivers/hooks/useUserActionProvider';
import Layout from '../layout/Layout';

export default function App() {
  const section = useSelector((state:State) => state.section);
  const {eventActionProvider} = useEventActionProvider();
  const {userActionProvider} = useUserActionProvider();

  const setInitialEvents = () => {
    const events = new MockEventActionsRepository().mockEvents();
    events.forEach((item) => eventActionProvider.addEvent(item));
  };
  
  const setInitialUsers = () => {
    const users = new MockUserActionsRepository().mockUsers();
    users.forEach((user) => userActionProvider.addUser(user));
  };

  useEffect(() => {
    LocalStorageRepository.clear();
    setInitialEvents();
    setInitialUsers();
  }, []);
  
  return (
    <Layout>
      { section === 'Events' ?
        <EventSection /> :
        <UserSection />
      }
    </Layout>
  )
}
