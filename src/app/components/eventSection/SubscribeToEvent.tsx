import React, {useState} from 'react';
import {Flex, Text, Input} from '@chakra-ui/react';
import {Event} from '../../../core/entities/Event';
import {ActionButton} from '../../system/ActionButton/ActionButton';

export interface SubscribeToEventProps {
  event?: Event
  onSubscription: (subscribeUserId: string) => void
  onClose: () => void
}

export default function SubscribeToEvent({event, onSubscription, onClose}: SubscribeToEventProps) {
  const [subscribeUserId, setSubscribeUserId] = useState('');

  const handleSubscription = () => {
    if (!subscribeUserId) { return; }
    onSubscription(subscribeUserId);
    setSubscribeUserId('')
  }
  
  const handleOnChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
    setSubscribeUserId(ev.target.value);
  }

  return (
    <Flex direction="column" m="0 50px" border="2px dashed #5A4998" p="20px" h="250px" w="280px">
      <Text mb="10px">Subscribe to: <b>{event?.name}</b></Text>
      <Input type="text" placeholder='Insert Attendee ID' value={subscribeUserId} onChange={handleOnChange} p="10px" mb="10px"/>
      <ActionButton name="Subscribe" width="100%" color="white" hover={{backgroundColor: '#3D307B'}}
        background='#5A4998' height='50px' margin={'10px 0'} onClick={handleSubscription}/>
      <ActionButton name="Close" fontSize="0.9rem" width="100%" color="tomato" onClick={onClose} hover={{border: '1px solid tomato'}}/>  
    </Flex>
  );
};
