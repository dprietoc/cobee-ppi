import {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {Flex, Box, HStack, Text} from '@chakra-ui/react';
import {AddIcon} from '@chakra-ui/icons';
import {Event} from '../../../core/entities/Event';
import {setSection} from '../../reducers/menuReducer';
import useToast from '../../hooks/useToast';
import useEventActionProvider from '../../../core/drivers/hooks/useEventActionProvider';
import useUserActionProvider from '../../../core/drivers/hooks/useUserActionProvider';
import EventCard from '../../system/EventCard/EventCard';
import EventForm from '../eventForm/EventForm';
import SubscribeToEvent from './SubscribeToEvent';

export default function EventSection() {
  const [events, setEvents] = useState<Event[]>([]);
  const [eventUpdated, setEventUpdated] = useState<Event>();
  const [eventSubscribed, setEventSubscribed] = useState<Event>();
  const [isOpenSubscription, setOpenSubscription]  = useState(false);
  const [isOpenFormEvent, setOpenFormEvent]  = useState(false);
  const {eventActionProvider} = useEventActionProvider();
  const {userActionProvider} = useUserActionProvider();
  const dispatch = useDispatch();
  const {renderToast} = useToast();

  const getEvents = () => {
    const localEvents = eventActionProvider.getAllEvents();
    setEvents(localEvents);
  };

  const handleEventAdded = (event: Event) => {
    if (events.find((item) => item.eventId === event.eventId)) {
      renderToast('error', 'Error', `EventId #${event.eventId} selected is duplicated!`);
    } else {
      const newEvent = eventActionProvider.addEvent(event);
      renderToast('success', 'Success', `Event #${newEvent.eventId} was added!`);
    }
    setOpenFormEvent(false);
    getEvents();
  }

  const handleEventModify = (event: Event) => {
    const updatedEvent = eventActionProvider.updateEvent(event);
    renderToast('success', 'Success', `Event #${updatedEvent.eventId} was modified!`);
    setOpenFormEvent(false);
    getEvents();
  }

  const handleEventSubscription = (subscribeUserId: string) => {
    const user = userActionProvider.getUser(subscribeUserId);
    if (!user) {
      renderToast('error', 'Error', `UserId #${subscribeUserId} not found!`);
      return;
    }
    userActionProvider.addToEvent(subscribeUserId, eventSubscribed!);
    renderToast('success', 'Success', `UserId #${subscribeUserId} was subscribed to ${eventSubscribed?.name}!`);
    setOpenSubscription(false);
    dispatch(setSection('Attendees'));
  }
  
  const handleOnAddEvent = () => {
    setOpenSubscription(false);
    setEventUpdated(undefined);
    setOpenFormEvent(true);
  }

  const handleOnDelete = (eventId: string) => {
    setOpenSubscription(false);
    const updatedEvents = eventActionProvider.removeEvent(eventId);
    renderToast('success', 'Success', `Event #${eventId} was deleted!`);
    setEvents(updatedEvents);
  }

  const handleOnModify = (eventId: string) => {
    setOpenSubscription(false);
    setOpenFormEvent(false);
    const eventForUpdate = eventActionProvider.getEvent(eventId);
    setEventUpdated(eventForUpdate);
    setOpenFormEvent(true);
  }

  const handleOnSubscribe = (event: Event) => {
    setOpenFormEvent(false);
    setOpenSubscription(true);
    setEventSubscribed(event);
  }

  useEffect(()=>{
    getEvents();
  }, []);

  return (
    <Flex justify="center" w="100%">
      { !isOpenSubscription && !isOpenFormEvent && (
        <Flex direction="column">
          <HStack onClick={handleOnAddEvent} w="100%" justify="right" cursor="pointer" color="#666" p="0 50px">
            <AddIcon mr="5px" mt="3px"/><Text>Add Event</Text>
          </HStack>
          <Flex m="2rem" wrap={'wrap'}>
            {events.length ? 
              events.map((event) => (
                <Box key={event.eventId}>
                  <EventCard event={event} onDelete={handleOnDelete} onModify={handleOnModify} onSubscribe={handleOnSubscribe}/>
                </Box>
              )) : ''
            }
          </Flex>
        </Flex>
        )}
        { isOpenSubscription && <SubscribeToEvent event={eventSubscribed} onSubscription={handleEventSubscription} onClose={() => setOpenSubscription(false)} />}
        { isOpenFormEvent && <EventForm event={eventUpdated} onSubmit={eventUpdated ? handleEventModify : handleEventAdded} onClose={() => setOpenFormEvent(false)} />}
    </Flex>
  );
};
