import { Box, Flex, Heading } from '@chakra-ui/react';
import {useDispatch} from 'react-redux';
import {setSection} from '../../reducers/menuReducer';
import Menu from '../../system/Menu/Menu';

export interface LayoutProps {
  children: any
}

export default function Layout({ children }: LayoutProps) {
  const dispatch = useDispatch();

  const handleMenuOnClick = (option: string) => {
    dispatch(setSection(option));
  };

  return (
    <Box w={'100vw'} h={'100vh'} backgroundColor={'#EEE'} p="20px">
      <Heading fontSize="2xl" mb="5rem">
        Conference Management
      </Heading>
      <Flex h={'80vh'} m={'auto 0'}>
        <Menu options={['Events', 'Attendees']} onOptionClick={handleMenuOnClick}/>
        {children}
      </Flex>
    </Box>
  );
};
