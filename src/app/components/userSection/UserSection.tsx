import {useEffect, useState} from 'react';
import {Flex, Box} from '@chakra-ui/react';
import {User} from '../../../core/entities/User';
import UserCard from '../../system/UserCard/UserCard';
import useUserActionProvider from '../../../core/drivers/hooks/useUserActionProvider';
import useToast from '../../hooks/useToast';

export default function UserSection() {
  const [users, setUsers] = useState<User[]>([]);
  const {userActionProvider} = useUserActionProvider();
  const {renderToast} = useToast();

  const getUsers = () => {
    const localUsers = userActionProvider.getAllUsers();
    setUsers(localUsers);
  };

  const handleOnEventDelete = (userId: string, eventId: string) => {
    userActionProvider.removeFromEvent(userId, eventId);
    renderToast('success', 'Success', `User #${userId} was unsubscribed from #${eventId} event!`);
    getUsers();
  }
  
  useEffect(()=>{
    getUsers();
  }, []);

  return (
    <Flex direction="column" w="100%">
      {users.length ? 
        users.map((user) => (
          <Box key={user.userId}>
            <UserCard {...user} onEventDelete={handleOnEventDelete}/>
          </Box>
        )) : ''
      }
    </Flex>
  );
};
