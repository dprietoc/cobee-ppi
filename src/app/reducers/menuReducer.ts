export type State = {
  section: 'Events' | 'Attendees'
}

type Action = {
  type: string,
  payload: any
}

const initialState:State = {
  section: 'Events',
};

export const menuActions = {
  SET_SECTION: 'SET_SECTION',
};

export const setSection = (payload: any) => ({
  type: menuActions.SET_SECTION,
  payload,
});


const actions: {[key:string]: (state:State, action: Action) => State} = {
  [menuActions.SET_SECTION]: (state, action) => {
    return {...state, section: action.payload};
  },
};

export const menuReducer = (state = initialState, action: Action) => {
  return actions[action.type]?.(state, action) ?? state;
};
