
import {useToast} from '@chakra-ui/react';

export default () => {
  const toast = useToast();
  const renderToast = (type: 'success' | 'error', title: string, message: string) => {
    toast({
      description: message,
      position: 'bottom',
      duration: 5000,
      isClosable: true,
      status: type,
      title: title,
    });
  };
  return {renderToast};
};
