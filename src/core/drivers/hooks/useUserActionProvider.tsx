import {IUserRepository} from '../../adapters/repositories/IUserRepository';
import Configuration from '../Configuration';
import {UserActions} from '../../interactors/UserActions';
import {APILocalUserActionsRepository} from '../repositories/userActions/APILocalUserActionsRepository';

export default () => {
  const userActionsRepository: IUserRepository = new APILocalUserActionsRepository();
  const userActionProvider: UserActions = Configuration.userActionsWithRepository(userActionsRepository);
  return {userActionProvider};
}