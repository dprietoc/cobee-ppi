import {IEventRepository} from '../../adapters/repositories/IEventRepository';
import Configuration from '../Configuration';
import {EventActions} from '../../interactors/EventActions';
import {APILocalEventActionsRepository} from '../repositories/eventActions/APILocalEventActionsRepository';

export default () => {
  const eventActionsRepository: IEventRepository = new APILocalEventActionsRepository();
  const eventActionProvider: EventActions = Configuration.eventActionsWithRepository(eventActionsRepository);
  return {eventActionProvider};
}