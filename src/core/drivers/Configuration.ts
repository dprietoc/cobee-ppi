import { EventActions } from '../interactors/EventActions';
import { UserActions } from '../interactors/UserActions';
import { IEventRepository } from '../adapters/repositories/IEventRepository';
import { IUserRepository } from '../adapters/repositories/IUserRepository';
import { MockEventActionsRepository } from './repositories/eventActions/MockEventActionsRepository';
import { MockUserActionsRepository } from './repositories/userActions/MockUserActionsRepository';

class Configuration {
  static eventActionsWithRepository(repository: IEventRepository): EventActions {
    return new EventActions(repository);
  }

  static eventActions() {
    return this.eventActionsWithRepository(new MockEventActionsRepository());
  }

  static userActionsWithRepository(repository: IUserRepository): UserActions {
    return new UserActions(repository);
  }

  static userActions() {
    return this.userActionsWithRepository(new MockUserActionsRepository());
  }
}

export default Configuration;