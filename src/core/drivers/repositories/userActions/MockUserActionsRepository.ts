import { IUserRepository } from "../../../adapters/repositories/IUserRepository";
import { User } from '../../../entities/User';
import { Event } from '../../../entities/Event'; 

export class MockUserActionsRepository implements IUserRepository {
  addUser(user: User): User {
    return this.mockUsers()[0];
  }
  addToEvent(userId: string, event: Event): User {
    return this.mockUsers()[0];
  }
  get(userId: string): User {
    return this.mockUsers()[0];
  }
  getAll(): User[] {
    return this.mockUsers();
  }
  removeToEvent(userId: string, eventId: string): User {
    return this.mockUsers()[0];
  }

  mockUsers(): User[] {
    return [
      User.builder()
        .name('Joe')
        .email('joe@mail.com')
        .userId('1234')
        .eventList([])
        .build(),
      User.builder()
        .name('Sarah')
        .email('sarah@mail.com')
        .userId('2345')
        .eventList([])
        .build(),
      User.builder()
        .name('Evans')
        .email('evans@mail.com')
        .userId('3456')
        .eventList([])
        .build(),
    ]
  }
}