import {IUserRepository} from '../../../adapters/repositories/IUserRepository';
import {LocalStorageRepository} from '../LocalStorageRepository';
import {User} from '../../../entities/User';
import {Event} from '../../../entities/Event';

export class APILocalUserActionsRepository extends LocalStorageRepository implements IUserRepository {
  addUser(user: User): User {
    const allUsers = this.getAll();
    allUsers.push(user);
    this.setItem({key: 'users', value: allUsers});
    return user;
  }
  
  addToEvent(userId: string, event: Event): User {
    const selectedUser = this.get(userId);
    if (selectedUser.eventList?.length) {
      const isEventAdded = selectedUser.eventList.find((item) => item.eventId === event.eventId);
      if (isEventAdded) return selectedUser;
    }
    selectedUser.eventList?.push(event);
    const updatedUsers = this.getAll().map((user) => (user.userId === userId ? selectedUser : user));
    this.setItem({key: 'users', value: updatedUsers});
    return selectedUser;
  }

  get(userId: string): User {
    const allUsers = this.getAll();
    const userSelected = allUsers.filter((user) => user.userId === userId)[0];
    return userSelected;
  }

  getAll(): User[] {
    const allUsers: User[] = this.getItem('users');
    return allUsers ?? [];
  }

  removeFromEvent(userId: string, eventId: string): User {
    const selectedUser = this.get(userId);
    selectedUser.eventList = selectedUser.eventList?.filter((event) => event.eventId !== eventId);
    const updatedUsers = this.getAll().map((user) => (user.userId === userId ? selectedUser : user));
    this.setItem({key: 'users', value: updatedUsers});
    return selectedUser;
  }
}
