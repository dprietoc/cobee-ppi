interface ItemType {
  [key: string]: any,
};

export class LocalStorageRepository {
  static clear() {
    return localStorage.clear();
  }
  deleteItem(key: string) {
    return localStorage.deleteItem(key);
  } 
  getItem(key: string) {
    const value = localStorage.getItem(key);
    return value ? JSON.parse(value) : null;
  }
  setItem({key, value}: ItemType) {
   return localStorage.setItem(key, JSON.stringify(value));
  }
}
