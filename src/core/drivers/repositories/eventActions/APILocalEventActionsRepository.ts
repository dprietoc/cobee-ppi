import {IEventRepository} from '../../../adapters/repositories/IEventRepository';
import {LocalStorageRepository} from '../LocalStorageRepository';
import {Event} from '../../../entities/Event';

export class APILocalEventActionsRepository extends LocalStorageRepository implements IEventRepository {
  add(event: Event): Event {
    const allEvents = this.getAll();
    allEvents.push(event);
    this.setItem({key: 'events', value: allEvents});
    return event;
  }

  get(eventId: string): Event {
    const allEvents = this.getAll();
    const eventSelected = allEvents.filter((event) => event.eventId === eventId)[0];
    return eventSelected;
  }

  getAll(): Event[] {
    const allEvents: Event[] = this.getItem('events');
    return allEvents ?? [];
  }

  remove(eventId: string): Event[] {
    const allEvents = this.getAll();
    const updatedEvents = allEvents.filter((event) => event.eventId !== eventId);
    this.setItem({key: 'events', value: updatedEvents});
    return updatedEvents;
  }

  update(event: Event): Event {
    const allEvents = this.getAll();
    const updatedEvents =  allEvents.filter((value) => value.eventId !== event.eventId);
    updatedEvents.push(event);
    this.setItem({key: 'events', value: updatedEvents});
    return event;
  }
}
