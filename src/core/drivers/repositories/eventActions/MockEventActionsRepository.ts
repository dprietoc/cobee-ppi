import { IEventRepository } from "../../../adapters/repositories/IEventRepository";
import { Event } from '../../../entities/Event';

export class MockEventActionsRepository implements IEventRepository {
  add(event: Event): Event{
    return this.mockEvents(event.eventId)[0];
  }

  get(evenId: string): Event {
    return this.mockEvents()[0];
  }

  getAll(): Event[] {
    return this.mockEvents();
  }

  remove(eventId: string): Event[]{
    return this.mockEvents(eventId);
  }

  update(event: Event): Event{
    return this.mockEvents()[0];
  }

  mockEvents(id?: string): Event[] {
    return [
      Event.builder()
        .name('First Event')
        .description('Nice description of 1st event')
        .date('10/05/22')
        .eventId('1234')
        .location('Barcelona')
        .build(),
      Event.builder()
        .name('Second Event')
        .description('Nice description of 2nd event')
        .date('12/06/22')
        .eventId('2345')
        .location('Madrid')
        .build(),
      Event.builder()
        .name('Third Event')
        .description('Nice description of 3rd event')
        .date('30/08/22')
        .eventId('3456')
        .location('Valencia')
        .build(),
    ]
  }
}