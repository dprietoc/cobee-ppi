import { Event } from '../entities/Event';
import { IEventRepository } from "../adapters/repositories/IEventRepository";

export class EventActions {
  private repository: IEventRepository

  constructor(repository: IEventRepository) {
    this.repository = repository;
  }

  addEvent(event: Event) {
    return this.repository.add(event);
  }

  getEvent(eventId: string) {
    return this.repository.get(eventId)
  }
  
  getAllEvents() {
    return this.repository.getAll();      
  }

  removeEvent(eventId: string) {
    return this.repository.remove(eventId);
  }

  updateEvent(event: Event) {
    return this.repository.update(event);
  }
}