import { Event } from '../entities/Event';
import { User} from '../entities/User';
import { IUserRepository } from '../adapters/repositories/IUserRepository';

export class UserActions {
  private repository: IUserRepository

  constructor(repository: IUserRepository) {
    this.repository = repository;
  }

  addUser(user: User) {
    return this.repository.addUser(user);
  }

  addToEvent(userId: string, event: Event) {
    return this.repository.addToEvent(userId, event);
  }

  getUser(userId: string) {
    return this.repository.get(userId);
  }
    
  getAllUsers() {
    return this.repository.getAll();      
  }

  removeFromEvent(userId: string, eventId: string) {
    return this.repository.removeFromEvent(userId, eventId);
  }
}
