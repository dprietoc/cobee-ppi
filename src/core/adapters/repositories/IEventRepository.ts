import {Event} from '../../entities/Event';

export interface IEventRepository {
  add(event: Event): Event
  get(eventId: string): Event
  getAll(): Event[]
  remove(eventId: string): Event[]
  update(event: Event): Event
}