import {User} from '../../entities/User';
import {Event} from '../../entities/Event'

export interface IUserRepository {
  addUser(user: User): User
  addToEvent(userId: string, event: Event): User
  removeFromEvent(userId: string, eventId: string): User
  get(userId: string): User
  getAll(): User[]
}