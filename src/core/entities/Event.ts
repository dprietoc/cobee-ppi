export class Event {
  eventId: string
  date?: string
  description?: string
  name?: string
  location?: string

  constructor() {
    this.eventId = '';
  }

  static builder(): EventBuilder {
    return new EventBuilder();
  }
}

export class EventBuilder {
  private _eventId: string;
  private _date?: string;
  private _description?: string;
  private _name?: string;
  private _location?: string;

  constructor() {
    this._eventId = '';
  }

  eventId(eventId: string): EventBuilder {
    this._eventId = eventId;
    return this;
  }

  date(date: string): EventBuilder {
    this._date = date;
    return this;
  }

  description(description: string): EventBuilder {
    this._description = description;
    return this;
  }
  
  name(name: string): EventBuilder {
    this._name = name;
    return this;
  }

  location(location: string): EventBuilder {
    this._location = location;
    return this;
  }

  build(): Event {
    const event = new Event();
    event.eventId = this._eventId;
    event.date = this._date;
    event.description = this._description;
    event.name = this._name;
    event.location = this._location;
    return event;
  }

}