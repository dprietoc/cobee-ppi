import {Event} from './Event';

export class User {
  userId: string;
  name?: string;
  email?: string;
  eventList?: Event[];

  constructor() {
    this.userId = '';
  }

  static builder(): UserBuilder {
    return new UserBuilder();
  }
}

export class UserBuilder {
  private _userId: string;
  private _name?: string;
  private _email?: string;
  private _eventList?: Event[];

  constructor() {
    this._userId = '';
  }

  userId(userId: string): UserBuilder {
    this._userId = userId;
    return this;
  } 

  name(name: string): UserBuilder {
    this._name = name;
    return this;
  }

  email(email: string): UserBuilder {
    this._email = email;
    return this;
  }

  eventList(eventList: Event[]): UserBuilder {
    this._eventList = eventList;
    return this;
  }

  build(): User {
    const user = new User();
    user.userId = this._userId;
    user.name = this._name;
    user.email = this._email;
    user.eventList = this._eventList;
    return user;
  }

}