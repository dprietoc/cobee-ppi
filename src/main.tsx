import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './app/components/app/App'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { ChakraProvider } from '@chakra-ui/react'
import {menuReducer} from './app/reducers/menuReducer'

const store = createStore(menuReducer);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Provider store={store}>
      <ChakraProvider>
        <App />
      </ChakraProvider>
    </Provider>
  </React.StrictMode>
)
